" Map the leader key to ","
let mapleader = ','

syntax enable

set number

filetype off

function! DoRemote(arg)
  UpdateRemotePlugins
endfunction

let g:airline_powerline_fonts = 1
" Plugin Section - Run PlugInstall to install
call plug#begin('~/.config/nvim/plugged')
Plug 'scrooloose/nerdtree'
Plug 'scrooloose/nerdcommenter'
Plug 'Raimondi/delimitMate'
Plug 'lilydjwg/colorizer'
Plug 'git@github.com:ctrlpvim/ctrlp.vim.git'
Plug 'airblade/vim-gitgutter'
Plug 'Shougo/deoplete.nvim', { 'do': function('DoRemote') }
Plug 'tpope/vim-surround'
Plug 'zchee/deoplete-jedi'
Plug 'craigemery/vim-autotag'
Plug 'justinmk/vim-sneak'
Plug 'mileszs/ack.vim'
Plug 'zchee/deoplete-go', { 'do': 'make'}
Plug 'fatih/vim-go'
Plug 'nathanaelkane/vim-indent-guides'
Plug 'neovimhaskell/haskell-vim'
Plug 'jistr/vim-nerdtree-tabs'
Plug 'neomake/neomake'
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'
Plug 'arcticicestudio/nord-vim'
Plug 'vimwiki/vimwiki'
call plug#end()

" Disable folding for markdown
let g:vim_markdown_folding_disabled = 1

filetype plugin indent on

set expandtab " Make sure that every file uses real tabs, not spaces
set shiftround  " Round indent to multiple of 'shiftwidth'
set smartindent " Do smart indenting when starting a new line
set autoindent  " Copy indent from current line, over to the new line

let s:tabwidth=4
exec 'set tabstop='    .s:tabwidth
exec 'set shiftwidth=' .s:tabwidth
exec 'set softtabstop='.s:tabwidth

let g:nerdtree_tabs_open_on_console_startup = 1
let NERDTreeShowHidden = 1

" NEOMAKE (PEP8 Linting)
autocmd! BufWritePost * Neomake

" E501 is line length of 80 characters
let g:neomake_python_flake8_maker = {
    \ 'args': ['--format=default'],
    \ 'errorformat':
        \ '%E%f:%l: could not compile,%-Z%p^,' .
        \ '%A%f:%l:%c: %t%n %m,' .
        \ '%A%f:%l: %t%n %m,' .
        \ '%-G%.%#',
    \ }
let g:neomake_python_enabled_makers = ['flake8', 'pep8']


" Set line numbers to relative when in normal mode
autocmd InsertEnter * :set number
autocmd InsertLeave * :set relativenumber

" show a visual line under the cursor's current line
set cursorline

" show the matching part of the pair for [] {} and ()
set showmatch

" Configure ack.vim to use Ag if available
if executable('ag')
  let g:ackprg = 'ag --nogroup --nocolor --column'
endif

let g:rbpt_colorpairs = [
    \ ['brown',       'RoyalBlue3'],
    \ ['Darkblue',    'SeaGreen3'],
    \ ['darkgray',    'DarkOrchid3'],
    \ ['darkgreen',   'firebrick3'],
    \ ['darkcyan',    'RoyalBlue3'],
    \ ['darkred',     'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['brown',       'firebrick3'],
    \ ['gray',        'RoyalBlue3'],
    \ ['black',       'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['Darkblue',    'firebrick3'],
    \ ['darkgreen',   'RoyalBlue3'],
    \ ['darkcyan',    'SeaGreen3'],
    \ ['darkred',     'DarkOrchid3'],
    \ ['red',         'firebrick3'],
    \ ]

" Colorizer
let g:colorizer_fgcontrast = 0

" Autocomplete
let g:deoplete#enable_at_startup = 1
let g:deoplete#sources#go#gocode_binary = '/Users/wduss/bin/gocode'
inoremap <silent><expr> <Tab> pumvisible() ? "\<C-n>" : deoplete#mappings#manual_complete()
inoremap <Leader><Tab> <Space><Space><Space><Space>

let deoplete#sources#jedi#enable_cache = 1
let deoplete#sources#jedi#show_docstring = 0
set completeopt-=preview

" Ctrl+P Config
" Open file menu
nnoremap <Leader>o :CtrlP<CR>
" Open buffer menu
nnoremap <Leader>b :CtrlPBuffer<CR>
" Open most recently used files
nnoremap <Leader>f :CtrlPMRUFiles<CR>
:tnoremap <Esc> <C-\><C-n>

"set t_Co=256
set background=dark
set splitbelow
set splitright

au FocusGained,BufEnter * :silent! !

colorscheme nord

